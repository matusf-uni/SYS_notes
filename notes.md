# Systems

## Assembly & Machine Language
### Registers
- arithmetic can be done only in registers
- **R0** contains 0
- **R15** holds status

### Memory
- store values permanently

### RRR instructions
- Machine code:
  - *{op code}{dest}{reg}{reg}*
  - 0567 == add R5,R6,R7

|op code|name|arguments         |
|-------|----|------------------|
|0|add |dest_reg,reg1,reg2|
|1|sub |dest_reg,reg1,reg2|
|2|mul |dest_reg,reg1,reg2|
|3|div |dest_reg,reg1,reg2|
|d|trap|reg,reg,reg       |

### RX instructions
- Machine code
  - *f{reg}{index reg}{op code} {address}{}{}{}*

|op code|name|arguments|
|-------|----|---------|
|0|lea |dest_reg,val[reg]|
|1|load|dest_reg,mem[reg]|
|2|store|reg,dest_mem[reg]|
